const getContent = (elName: string) => {
  return [...document.querySelectorAll(elName)].map((v: any) => v.src).filter((v: string) => v);
}

const onMessages = (request: any, sender: any, sendResponse: any) => {
  sendResponse({ data: getContent(request.message.replace('get:', '')), ...request });
}

chrome.runtime.onMessage.addListener(onMessages);