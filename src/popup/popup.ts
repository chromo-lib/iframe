import onMessages from "./onMessages";
import { TResponse } from "./type";

const pre = document.querySelector('pre');
const btnControls = [...document.querySelectorAll('button')];

function createList(data: string[]) {
  const list = document.getElementById('list');
  list!.innerHTML = '';

  data.forEach((url: string, i: number) => {
    const li = document.createElement('li');
    const small = document.createElement('small');
    const spanT = document.createElement('span');

    small.classList.add('mr-1', 'gray');
    small.textContent = '(' + (i + 1) + ') ';
    spanT.textContent = url;
    spanT.classList.add('truncate');
    li.classList.add('d-flex', 'align-center');
    li.title = url;

    li.appendChild(small);
    li.appendChild(spanT);
    list?.appendChild(li);
  });
}

const local = localStorage.getItem('data');
if (local) createList(JSON.parse(local));

btnControls.forEach(btn =>
  btn.addEventListener('click', (e: any) => {
    const message = e.target.id.replace('btn-', '');
    btnControls.forEach(b => b.classList.remove('active'));
    e.target.classList.add('active');

    chrome.tabs.query({ currentWindow: true, active: true }, function (tabs) {
      const tab: chrome.tabs.Tab = tabs[0];

      if (tab.url?.startsWith('edge') || tab.url?.startsWith('firefox') || tab.url?.startsWith('chrome')) {
        pre!.textContent = 'Page is not supported..';
        pre!.style.display = 'block';
        setTimeout(() => pre!.style.display = 'none', 3000);
        return;
      }

      chrome.tabs.sendMessage(+tab!.id!, { message }, (response: TResponse) => {
        if (response.data && response.data.length > 0) {
          createList(response.data);
          localStorage.setItem('data', JSON.stringify(response.data));
          return;
        }
        else {
          pre!.textContent = `No ${message.replace('get:', '')} was found..`;
          pre!.style.display = 'block';
          setTimeout(() => pre!.style.display = 'none', 3000);
        }
      });
    });

  })
)

chrome.runtime.onMessage.addListener(onMessages);
chrome.runtime.setUninstallURL('https://haikel-fazzani.deno.dev');